package models

type Account struct {
	Id       string `json:"_id,omitempty" bson:"_id,omitempty"`
	Username string `json:"username,omitempty" bson:"username,omitempty"`
	Email    string `json:"email,omitempty" bson:"email,omitempty"`
	Password string `json:"password,omitempty" bson:"password,omitempty"`
	Name     string `json:"name,omitempty" bson:"name,omitempty"`
	Surname  string `json:"surname,omitempty" bson:"surname,omitempty"`
	Age      int    `json:"age,omitempty" bson:"age,omitempty"`
	Gender   string `json:"gender,omitempty" bson:"gender,omitempty"`
}
