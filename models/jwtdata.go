package models

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type JWTData struct {
	jwt.StandardClaims
	User JWTAccount `json:"user,omitempty"`
}

type JWTAccount struct {
	Id       string `json:"_id,omitempty" bson:"_id,omitempty"`
	Username string `json:"username,omitempty" bson:"username,omitempty"`
	Email    string `json:"email,omitempty" bson:"username,omitempty"`
	Name     string `json:"name,omitempty" bson:"username,omitempty"`
	Surname  string `json:"surname,omitempty" bson:"username,omitempty"`
	Age      int    `json:"age,omitempty" bson:"username,omitempty"`
	Gender   string `json:"gender,omitempty" bson:"username,omitempty"`
}

func NewJWTData(user JWTAccount, expired bool) *JWTData {
	jwtData := &JWTData{
		User: user,
	}
	if expired {
		jwtData.StandardClaims.ExpiresAt = time.Now().Add(time.Hour).Unix()
	}
	return jwtData
}
