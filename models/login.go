package models

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	AccessToken  string `json:"access_token,omitempty"`
	RefreshToken string `json:"refresh_token,omitempty"`
}

type TokenWrite struct {
	RefreshToken string `bson:"refresh_token,omitempty" json:"refresh_token,omitempty"`
	AccountId    string `bson:"account_id,omitempty" json:"account_id,omitempty"`
}

// TODO: DELETE SESSION
type RefreshTokenReq struct {
	RefreshToken string `json:"refresh_token,omitempty"`
}
