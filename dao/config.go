package dao

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const dbURI = "mongodb+srv://rentup:1234.rentup@rentup-db-rchgg.mongodb.net/test?retryWrites=true&w=majority"

func Init() (*mongo.Client) {
	client, err := mongo.NewClient(options.Client().ApplyURI(dbURI))
	if err != nil {
		fmt.Println("ERROR AL CREAR CLIENTE DE MONGODB", err)
		return nil
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		fmt.Println("ERROR AL CONECTARSE CON MONGODB")
		return nil
	}

	return client
}
