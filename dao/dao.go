package dao

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type MongoDAO struct {
	Collection *mongo.Collection
}

func (dao MongoDAO) Get(ctx *gin.Context, filter bson.M, entity interface{}) error {
	context, _ := context.WithTimeout(context.Background(), 5*time.Second)
	if err := dao.Collection.FindOne(context, filter).Decode(entity); err != nil {
		return err
	}
	return nil
}

func (dao MongoDAO) Save(ctx *gin.Context, entity interface{}) error {
	context, _ := context.WithTimeout(context.Background(), 5*time.Second)
	item, _ := bson.Marshal(entity)
	_, err := dao.Collection.InsertOne(context, item)
	if err != nil {
		return err
	}
	return nil
}

func (dao MongoDAO) Update(ctx *gin.Context, filter bson.M, entity interface{}) error {
	item := bson.M{
		"$set": entity,
	}
	context, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, err := dao.Collection.UpdateOne(context, filter, item)
	if err != nil {
		return err
	}
	return nil

}

func (dao MongoDAO) Delete(ctx *gin.Context, filter bson.M) error {
	context, _ := context.WithTimeout(context.Background(), 5*time.Second)
	_, err := dao.Collection.DeleteOne(context, filter)
	if err != nil {
		fmt.Println("ERROR AL REMOVER USUARIO EN BD", err)
		return err
	}
	return nil
}
