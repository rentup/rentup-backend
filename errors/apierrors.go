package apierrors

type ApiError interface {
	Message() string
	Status() int
	Code() string
	Error() string
}

type apiErr struct {
	ErrorStatus  int    `json:"status"`
	ErrorCode    string `json:"code"`
	ErrorMessage string `json:"message"`
}

func (e apiErr) Message() string {
	return e.ErrorMessage
}

func (e apiErr) Status() int {
	return e.ErrorStatus
}

func (e apiErr) Code() string {
	return e.ErrorCode
}

func (e apiErr) Error() string {
	return e.ErrorCode
}

func NewApiError(message string, error string, status int) ApiError {
	return apiErr{status, error, message}
}
