package apierrors


const (
	// Invalid
	InvalidData string = "invalid_data"
	InvalidToken string = "invalid_access_token"
	InvalidPassword string = "invalid_password"

	// MongoDB
	MongoDBFindError string = "mongodb_find_error"
	MongoDBInsertError string = "mongodb_insert_error"
	MongoDBUpdateError string = "mongodb_update_error"
	MongoDBDeleteError string = "mongodb_delete_error"

	// Token
	TokenGetError string = "token_get_error"
	TokenCreateError string = "token_create_error"

	// Account
	AccountGetError string = "account_get_error"
	AccountCreateError string = "mongodb_insert_error"
	AccountUpdateError string = "mongodb_update_error"
	AccountDeleteError string = "mongodb_delete_error"
	AccounNotFound string = "account_not_found"
)
