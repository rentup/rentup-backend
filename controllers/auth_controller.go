package controllers

import (
	"github.com/gin-gonic/gin"
	apierrors "github.com/rentup/rentup-backend/errors"
	"github.com/rentup/rentup-backend/models"
	"github.com/rentup/rentup-backend/services"
	"github.com/rentup/rentup-backend/utils"
	"net/http"
)

type AuthController struct {
	AccountService services.AccountService
}

func (controller AuthController) Login(ctx *gin.Context) {
	login := &models.Login{}
	if err := ctx.BindJSON(login); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.InvalidData,
			http.StatusBadRequest,
		)
		ctx.JSON(validationErr.Status(), validationErr)
		return
	}
	loginResponse, err := controller.AccountService.Authenticate(ctx, login)
	if err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusOK, loginResponse)
}

func (controller AuthController) RefreshToken(ctx *gin.Context) {
	tokenReq := models.RefreshTokenReq{}
	ctx.Bind(&tokenReq)

	newToken, err := controller.AccountService.RefreshToken(ctx, tokenReq)
	if err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusOK, newToken)
}

func (controller AuthController) ValidateToken(ctx *gin.Context) {
	tokenString := utils.GetToken(ctx)
	if err := controller.AccountService.ValidateToken(tokenString); err != nil {
		ctx.AbortWithStatusJSON(err.Status(), err)
		return
	}
}
