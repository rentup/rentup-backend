package controllers

import (
	"github.com/gin-gonic/gin"
	apierrors "github.com/rentup/rentup-backend/errors"
	"github.com/rentup/rentup-backend/models"
	"github.com/rentup/rentup-backend/services"
	"net/http"
)

type AccountController struct {
	AccountService services.AccountService
}

func (controller AccountController) GetAccount(ctx *gin.Context) {
	accountId, _ := ctx.Params.Get("id")
	accountResponse, err := controller.AccountService.GetAccount(ctx, accountId)
	if err != nil {
		ctx.JSON(err.Status(), err)
	}
	ctx.JSON(http.StatusOK, accountResponse)
}

func (controller AccountController) CreateAccount(ctx *gin.Context) {
	newAccountRequest := &models.Account{}
	if err := ctx.BindJSON(newAccountRequest); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.InvalidData,
			http.StatusBadRequest,
		)
		ctx.JSON(validationErr.Status(), validationErr)
		return
	}
	account, err := controller.AccountService.CreateAccount(ctx, newAccountRequest)
	if err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusOK, account)
}

func (controller AccountController) UpdateAccount(ctx *gin.Context) {
	accountId, _ := ctx.Params.Get("id")
	updateAccountRequest := &models.Account{}
	if err := ctx.BindJSON(updateAccountRequest); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.InvalidData,
			http.StatusBadRequest,
		)
		ctx.JSON(validationErr.Status(), validationErr)
		return
	}
	account, err := controller.AccountService.UpdateAccount(ctx, accountId, updateAccountRequest)
	if err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusOK, account)
}

func (controller AccountController) DeleteUser(ctx *gin.Context) {
	accountId, _ := ctx.Params.Get("id")
	if err := controller.AccountService.DeleteAccount(ctx, accountId); err != nil {
		ctx.JSON(err.Status(), err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"code": "OK",
	})
}
