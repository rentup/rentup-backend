package main

import "github.com/rentup/rentup-backend/server"

func main() {
	server.New().Run(":8080")
}
