package server

import (
	"github.com/gin-gonic/gin"
	apierrors "github.com/rentup/rentup-backend/errors"
	"github.com/rentup/rentup-backend/utils"
	"net/http"
)

func ValidateTokenHeader() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := utils.GetToken(ctx)
		if token == "" {
			validationErr := apierrors.NewApiError(
				"Token missing",
				apierrors.InvalidData,
				http.StatusBadRequest,
			)
			ctx.AbortWithStatusJSON(validationErr.Status(), validationErr)
		}
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-Access-Token, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if ctx.Request.Method == "OPTIONS" {
			ctx.AbortWithStatus(204)
			return
		}

		ctx.Next()
	}
}
