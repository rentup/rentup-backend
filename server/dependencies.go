package server

import (
	"github.com/rentup/rentup-backend/controllers"
	"github.com/rentup/rentup-backend/dao"
	"github.com/rentup/rentup-backend/services"
	"go.mongodb.org/mongo-driver/mongo"
)

func resolveAccountController(client *mongo.Client) controllers.AccountController {
	accountCollection := client.Database("Test").Collection("Accounts")
	accountDBA := dao.MongoDAO{Collection: accountCollection}
	accountService := services.AccountService{AccountDBA: accountDBA}
	return controllers.AccountController{
		AccountService: accountService,
	}
}

func resolveAuthController(client *mongo.Client) controllers.AuthController {
	accountCollection := client.Database("Test").Collection("Accounts")
	tokenCollection := client.Database("Test").Collection("Tokens")
	accountDBA := dao.MongoDAO{Collection: accountCollection}
	tokenDBA := dao.MongoDAO{Collection: tokenCollection}
	accountService := services.AccountService{
		AccountDBA: accountDBA,
		TokenDBA:   tokenDBA,
	}
	return controllers.AuthController{
		AccountService: accountService,
	}
}
