package server

import (
	"github.com/gin-gonic/gin"
	"github.com/rentup/rentup-backend/dao"
)

// TODO: Mejorar manejo de errores de MongoDB. Actualmente solamente devolvemos el error message
// pero el error code y status siempre es bad request invalid data.
// TODO: Mejorar manejo de errores en Login y Refresh Token
// TODO: Definir como vamos a loguear los errores -  Ver DOC de gin
// TODO: Definir como vamos a manejar las respuestas

func New() *gin.Engine {
	// MongoDB client is create
	client := dao.Init()

	// Declaration of all CONTROLLERS
	accountController := resolveAccountController(client)
	authController := resolveAuthController(client)
	// Creation of SERVER
	router := gin.Default()

	router.Use(CORSMiddleware());

	// Declaration of all ROUTES - Posiblemente esto se haga en un archivo aparte

	// CRUD - USER
	router.GET("/user/:id",
		ValidateTokenHeader(),
		authController.ValidateToken,
		accountController.GetAccount,
	)

	router.POST("/user",
		accountController.CreateAccount,
	)

	router.PUT("/user/:id",
		ValidateTokenHeader(),
		authController.ValidateToken,
		accountController.UpdateAccount,
	)

	router.DELETE("/user/:id",
		ValidateTokenHeader(),
		authController.ValidateToken,
		accountController.DeleteUser,
	)

	// LOGIN -LOGOUT
	router.POST("/login",
		authController.Login)

	router.POST("/logout")

	// TOKEN
	router.POST("/token/refresh",
		authController.RefreshToken)

	return router
}
