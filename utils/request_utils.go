package utils

import (
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

const (
	CONTEXT_ACCESS_TOKEN string = "X-ACCESS-TOKEN"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GetToken(ctx *gin.Context) string {
	return getValueFromContext(ctx, CONTEXT_ACCESS_TOKEN)
}

func getValueFromContext(ctx *gin.Context, key string) string {
	value := ""
	exists := false
	var idFromContext interface{}
	if idFromContext, exists = ctx.Get(key); !exists {

		// Not in the Context, try yo get it from url.
		value = ctx.Query(key)

		if value == "" {
			// Not in the URL, try to get it from headers.
			if headerValue := ctx.Request.Header.Get(key); headerValue != "" {
				// It was in the HEADERS. Save it in the context.
				value = headerValue
				ctx.Set(key, value)
			}
		}  else {
			// It was in the URL. Save it in the context.
			ctx.Set(key, value)
		}
	} else {
		// It was already in the context.
		value = idFromContext.(string)
	}
	return value
}
