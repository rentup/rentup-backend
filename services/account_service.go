package services

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/rentup/rentup-backend/dao"
	"github.com/rentup/rentup-backend/errors"
	"github.com/rentup/rentup-backend/models"
	"github.com/rentup/rentup-backend/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

type AccountService struct {
	AccountDBA dao.MongoDAO
	TokenDBA   dao.MongoDAO
}

const (
	SECRET         = "123456a"
	SECRET_REFRESH = "123456A"
)

func (service *AccountService) GetAccount(ctx *gin.Context, id string) (*models.Account, apierrors.ApiError) {
	account := &models.Account{}
	filter := bson.M{
		"_id": id,
	}
	if err := service.AccountDBA.Get(ctx, filter, account); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.AccountGetError,
			http.StatusBadRequest,
		)
		return nil, validationErr
	}
	return account, nil
}

func (service *AccountService) CreateAccount(ctx *gin.Context, account *models.Account) (*models.Account, apierrors.ApiError) {
	id := (primitive.NewObjectID()).Hex()
	account.Id = id
	account.Password, _ = utils.HashPassword(account.Password)
	if err := service.AccountDBA.Save(ctx, account); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.AccountCreateError,
			http.StatusInternalServerError,
		)
		return nil, validationErr
	}
	return account, nil
}

func (service *AccountService) DeleteAccount(ctx *gin.Context, id string) apierrors.ApiError {
	filter := bson.M{
		"_id": id,
	}
	if err := service.AccountDBA.Delete(ctx, filter); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.AccountDeleteError,
			http.StatusInternalServerError,
		)
		return validationErr
	}
	return nil
}

func (service *AccountService) UpdateAccount(ctx *gin.Context, id string, account *models.Account) (*models.Account, apierrors.ApiError) {
	filter := bson.M{
		"_id": id,
	}
	if err := service.AccountDBA.Update(ctx, filter, account); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.AccountUpdateError,
			http.StatusInternalServerError,
		)
		return nil, validationErr
	}
	return account, nil
}

func (service *AccountService) Authenticate(ctx *gin.Context, request *models.Login) (*models.LoginResponse, apierrors.ApiError) {
	filter := bson.M{
		"$or": []bson.M{
			{"username": request.Username},
			{"email": request.Username},
		},
	}
	account := &models.Account{}
	if err := service.AccountDBA.Get(ctx, filter, account); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.AccountGetError,
			http.StatusBadRequest,
		)
		return nil, validationErr
	}
	if utils.CheckPasswordHash(account.Password, request.Password) {
		validationErr := apierrors.NewApiError(
			"The password is incorrect",
			apierrors.InvalidPassword,
			http.StatusBadRequest,
		)
		return nil, validationErr
	}

	accessToken, err := service.generateAccessToken(account)
	if err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.TokenCreateError,
			http.StatusInternalServerError,
		)
		return nil, validationErr
	}

	refreshToken, err := service.generateRefreshToken(ctx, account.Id)
	if err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.TokenCreateError,
			http.StatusInternalServerError,
		)
		return nil, validationErr
	}

	loginResponse := &models.LoginResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	return loginResponse, nil
}

func (service *AccountService) RefreshToken(ctx *gin.Context, tokenReq models.RefreshTokenReq) (*models.LoginResponse, apierrors.ApiError) {
	filter := bson.M{
		"refresh_token": tokenReq.RefreshToken,
	}
	tokenWrite := &models.TokenWrite{}
	if err := service.TokenDBA.Get(ctx, filter, tokenWrite); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.TokenGetError,
			http.StatusBadRequest,
		)
		return nil, validationErr
	}
	filter = bson.M{
		"_id": tokenWrite.AccountId,
	}
	account := &models.Account{}
	if err := service.AccountDBA.Get(ctx, filter, account); err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.AccountGetError,
			http.StatusBadRequest,
		)
		return nil, validationErr
	}
	token, err := service.generateAccessToken(account)
	if err != nil {
		validationErr := apierrors.NewApiError(
			err.Error(),
			apierrors.TokenCreateError,
			http.StatusInternalServerError,
		)
		return nil, validationErr
	}
	return &models.LoginResponse{
		AccessToken: token,
	}, nil

}

func (service *AccountService) ValidateToken(tokenString string) apierrors.ApiError {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(SECRET), nil
	})

	if err != nil || !token.Valid {
		validationErr := apierrors.NewApiError(
			"Access denied",
			apierrors.InvalidToken,
			http.StatusUnauthorized,
		)
		return validationErr
	}
	return nil
}

func (service *AccountService) generateAccessToken(account *models.Account) (string, error) {
	jwtAccount := models.JWTAccount{
		Id:       account.Id,
		Username: account.Username,
		Email:    account.Email,
		Name:     account.Name,
		Surname:  account.Surname,
		Age:      account.Age,
		Gender:   account.Gender,
	}

	claims := models.NewJWTData(jwtAccount, true)
	accessToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(SECRET))
	if err != nil {
		return "", err
	}

	return accessToken, nil
}

func (service *AccountService) generateRefreshToken(ctx *gin.Context, accountId string) (string, error) {
	refreshToken, err := jwt.New(jwt.SigningMethodHS256).SignedString([]byte(SECRET_REFRESH))
	tokenWrite := &models.TokenWrite{
		AccountId:    accountId,
		RefreshToken: refreshToken,
	}

	if err != nil {
		return "", err
	}
	filter := bson.M{
		"account_id": accountId,
	}
	//TODO: CREAR UN TOKEN_READ
	tokenRead := &models.TokenWrite{}
	if err := service.TokenDBA.Get(ctx, filter, tokenRead); err != nil {
		service.TokenDBA.Save(ctx, tokenWrite)
	} else {
		if err := service.TokenDBA.Update(ctx, filter, tokenWrite); err != nil {
			return "", err
		}
	}
	return refreshToken, nil
}
